const {ccclass, property} = cc._decorator;
 
@ccclass
export default class bulletEnemy extends cc.Component 
{
    
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    onload(){
        cc.director.getCollisionManager().enabled = true;
    }
    onBeginContact(contact, self, other){
        if(other.node.group == "player"){
            cc.audioEngine.playEffect(this.bgm, false);
            this.node.destroy();
        }
    }
    
}