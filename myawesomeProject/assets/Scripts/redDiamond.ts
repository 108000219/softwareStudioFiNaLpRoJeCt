import GameManager from "./GameManager";
const {ccclass, property} = cc._decorator;
 
@ccclass
export default class redDiamond extends cc.Component 
{
    @property(GameManager)
    gameMgr: GameManager = null;
    
    onload(){
        cc.director.getCollisionManager().enabled = true;
    }
    onBeginContact(contact, self, other)
    {
        
        if(other.node.name == "Girl")
        {
            this.gameMgr.score += 150; 
            this.node.active = false;
        }else{
            contact.disabled = true;
        }
    }
}