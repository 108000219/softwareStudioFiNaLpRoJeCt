import Playerboy_e from "./playerboy_e";
import Playergirl_e from "./playergirl_e";

const {ccclass, property} = cc._decorator;
 
@ccclass
export default class GameMgr_e extends cc.Component 
{
    
 

    @property(Playergirl_e)
    player_r: Playergirl_e = null;
    @property(Playerboy_e)
    player_l: Playerboy_e = null;
    

    @property({type:cc.Node})
    time: cc.Node = null;
    @property({type:cc.Node})
    nowscore: cc.Node = null;
    @property(cc.Node)
    enemyslife: cc.Node = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;
    
 
    private physicManager: cc.PhysicsManager = null;
    private second: number = 0;
    private minute: number = 0;
    private leftDown: boolean = false;
    private rightDown: boolean = false;
    private aDown: boolean = false;
    private dDown: boolean = false;
 
    public score: number = 2000;
 
    private highestScore: number = 0;
 
    private scoreCount;
 
    private pause: boolean = false;
 
    private playerLife: number = 12;
    
    public one: number = 0;
    public bullet_count = 5;
    public enemylife = 20;
    public playerhit = 0;
 
    onLoad()
    {
        
        // ===================== TODO =====================
        // 1. Enable physics manager 
        // 2. Set physics gravity to (0, -200)
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2 (0, -300);
        cc.audioEngine.playMusic(this.bgm, true);
        // ================================================
 
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        this.bullet_count = 5;
        this.enemylife = 20;
    }
 
    start(){
        cc.log(this.score);
        this.schedule(function(){
            if(this.second != 59)this.second += 1;
            else {
                this.second = 0;
                this.minute += 1;
            }
            let mstring = (this.minute < 10)?'0'+this.minute.toString():this.minute.toString();
            let sstring = (this.second < 10)?'0'+this.second.toString():this.second.toString();
            this.time.getComponent(cc.Label).string = mstring + ':' + sstring;
            if(this.minute < 1 && this.score >= 15)this.score -= 15;
            else if(this.minute < 3 && this.score >= 8)this.score -= 8;
            else if(this.score >= 3)this.score-=3;

        },1);
    }
    update(dt)
    {
        
        this.nowscore.getComponent(cc.Label).string = 'score : '+this.score.toString();
        this.enemyslife.getComponent(cc.Label).string = "enemy's life: "+ this.enemylife.toString();

        if(this.enemylife <= 0){
            cc.log("enemy die");
            this.end();
        }else if(this.player_r.door1() &&this.player_l.door1()){
            //this.dooropen1();
            this.end();
        }
        else if(this.player_r.door2() &&this.player_l.door2()){
           // this.dooropen2();
           this.end();
        }
        else if(this.player_r.door3() &&this.player_l.door3()){
            //this.dooropen3();
            this.end();
           
        }
        if(this.player_l.hit == 1 || this.player_r.hit ==1){

            cc.log("hit");
            cc.log("hit");
            cc.log("hit");
            cc.log("hit");
            this.score -= 50;
            this.player_l.hit = 0;
            this.player_r.hit = 0;

            if(this.score <= 0){
                this.scheduleOnce(function () {
                    // 这里的 this 指向 component
                    // cc.find('over').active = true;
                    //cc.game.restart();
                    cc.audioEngine.stopMusic();
                    cc.director.loadScene('failed');
                    cc.log("failed");
                }, 0.5);
            }
        }
    }
    end(){
        this.scheduleOnce(function () {
            cc.log(this.score);
            cc.audioEngine.stopMusic();
            
            if(this.score>3500){
                cc.director.loadScene('passa');
                cc.log("passa");
            }
            else if(this.score>2100){
                cc.director.loadScene('passb');
                cc.log("passb");
            }
            else{
                cc.director.loadScene('passc');
                cc.log("passc");
            }
            
            

        }, 0.5);
    }
    dooropen1(){//nobody
        this.scheduleOnce(function () {
            // 这里的 this 指向 component
            // cc.find('over').active = true;
            //cc.game.restart();
            cc.audioEngine.stopMusic();
            cc.director.loadScene('level.2');
            cc.log("level.2");
        }, 0.5);
    }
    dooropen2(){//nobody
        this.scheduleOnce(function () {
            // 这里的 this 指向 component
            // cc.find('over').active = true;
            //cc.game.restart();
            cc.audioEngine.stopMusic();
            cc.director.loadScene('level.3');
            cc.log("level.3");
        }, 0.5);
    }
    dooropen3(){//nobody
        this.scheduleOnce(function () {
            // 这里的 this 指向 component
            // cc.find('over').active = true;
            //cc.game.restart();
            cc.audioEngine.stopMusic();
            cc.director.loadScene('test');
            cc.log("level.1");
        }, 0.5);
    }
 
   
    onKeyDown(event)
    {
        cc.log("key:", event.keyCode)
        switch(event.keyCode)
        {
            case cc.macro.KEY.left:
                this.leftDown = true;
                this.player_r.playerMove(-1);
                this.player_r.move_l = true;
                break;
            case cc.macro.KEY.right:
                this.rightDown = true;
                this.player_r.move_r = true;
                this.player_r.playerMove(1);
                break;
            case cc.macro.KEY.up:
                this.player_r.playerJump(220);
                this.player_r.move_j = true;
                break;
            case cc.macro.KEY.a:
                this.aDown = true;
                this.player_l.playerMove(-1);
                this.player_l.move_l = true;
                break;
            case cc.macro.KEY.d:
                this.dDown = true;
                this.player_l.move_r = true;
                this.player_l.playerMove(1);
                break;
            case cc.macro.KEY.w:
                this.player_l.playerJump(220);
                this.player_l.move_j = true;
                break;
            case cc.macro.KEY.space:
                this.one = 1;
                break;
            
        }
    }
 
    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.left:
                this.leftDown = false;
                this.player_r.move_l = false;
                if(this.rightDown)
                    this.player_r.playerMove(1);
                else
                    this.player_r.playerMove(0);
                break;
            case cc.macro.KEY.right:
                this.rightDown = false;
                this.player_r.move_r = false;
                if(this.leftDown)
                    this.player_r.playerMove(-1);
                else
                    this.player_r.playerMove(0);
                break;
            case cc.macro.KEY.up:
                this.player_r.move_j = false;
                break;
            case cc.macro.KEY.a:
                this.aDown = false;
                this.player_l.move_l = false;
                if(this.dDown)
                    this.player_l.playerMove(1);
                else
                    this.player_l.playerMove(0);
                break;
            case cc.macro.KEY.d:
                this.dDown = false;
                this.player_l.move_r = false;
                if(this.aDown)
                    this.player_l.playerMove(-1);
                else
                    this.player_l.playerMove(0);
                break;
            case cc.macro.KEY.w:
                this.player_l.move_j = false;
                break;
            case cc.macro.KEY.space:
                this.one = 2;
                break;
        }
    }
 
    
    updateScore(score: number)
    {
        this.score = score;
        //this.scoreNode.getComponent(cc.Label).string = (Array(4).join("0") + this.score.toString()).slice(-4);
    }
 
    updateLife(num: number)
    {
        this.playerLife += num;
        this.playerLife = Math.min(Math.max(this.playerLife, 0), 12);
        //this.lifeBar.width = this.playerLife * 8
        if(this.playerLife == 0)
            this.gameOver();
    }
 
    gameStart()
    {
        
        this.player_r.node.active = true;
        this.schedule(this.scoreCount, 2);
        //cc.director.getCollisionManager().enabledDebugDraw = true;
        //cc.director.getCollisionManager().enabledDrawBoundingBox = true;
        // cc.audioEngine.playMusic(this.bgm, true);
    }
 
    
 
    gameOver()
    {
        this.player_r.node.active = false;
        // this.unschedule(this.scoreCount);
 
        //this.startIcon.active = true;
 
        // cc.audioEngine.stopMusic();
    }
 
    gameEnd()
    {
        cc.game.end();
    }

    
    
}
 