const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Switch_rl extends cc.Component 
{
    
    private anim: cc.Animation = null;
    private animateState = null;
    private pushed = 0;
    onload(){
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.node.getComponent(cc.Animation);
    }
    start () {
        this.anim = this.getComponent(cc.Animation);
    }
    onBeginContact(contact, self, other)
    {
        cc.log("push anum end");
        if(other.node.group == "ground")
        {
            this.node.y++;
        }
    }
    private afterPushed(){
        this.pushed = 1;
        this.animateState = this.anim.play('switchrl_e');
        cc.log("push anum end");
    }
}