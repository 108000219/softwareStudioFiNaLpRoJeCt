import GameMgr_e from "./gameMgrEnemy";

const {ccclass, property} = cc._decorator;
 
@ccclass
export default class spider extends cc.Component 
{
    @property(GameMgr_e)
    gameMgr: GameMgr_e = null;

    @property(cc.Prefab)
    syntax_err: cc.Prefab = null;
    @property(cc.Prefab)
    name_err: cc.Prefab = null;
    
    @property(cc.Node)
    enemybullet: cc.Node = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    private anim: cc.Animation = null;
    private animateState = null;

    onload(){
        cc.director.getCollisionManager().enabled = true;
        this.anim = this.node.getComponent(cc.Animation);
    }
    start(){

        this.anim = this.node.getComponent(cc.Animation);
        let action1 = cc.moveBy(0.5, 30, 115);
        let action2 = cc.moveBy(0.6, 110, -105);
        let action3 = cc.moveBy(0.4,10, 75);
        let action4 = cc.moveBy(0.5, -150, -25);
        let action5 = cc.moveBy(0.4, 0, -60);
        let action = cc.repeatForever(cc.sequence(action1, action2, action3, action4, action5));
        this.node.runAction(action);

        this.schedule(function(){

            let rand1 = Math.random();
            let rand2 = Math.random();
            let rand3 = Math.random();
            cc.log(rand1, rand2, rand3);
            cc.audioEngine.playEffect(this.bgm, false);
            let l1, l2, l3, l4;
            let r1, r2, r3;
            let u1, u2;

            // right
            if(rand1 <= 0.5){
                r1 = cc.instantiate(this.syntax_err);
                r2 = cc.instantiate(this.name_err);
                r3 = cc.instantiate(this.syntax_err);
            }else{
                r1 = cc.instantiate(this.name_err);
                r2 = cc.instantiate(this.syntax_err);
                r3 = cc.instantiate(this.syntax_err);
            }
            cc.log("r");
            // left
            if(rand2 <= 0.5){
                l1 = cc.instantiate(this.name_err);
                l2 = cc.instantiate(this.syntax_err);
                l3 = cc.instantiate(this.syntax_err);
                l4 = cc.instantiate(this.name_err);
            }else{
                l1 = cc.instantiate(this.name_err);
                l2 = cc.instantiate(this.syntax_err);
                l3 = cc.instantiate(this.name_err);
                l4 = cc.instantiate(this.syntax_err);
            }

            // up
            if(rand3 <= 0.5){
                u1 = cc.instantiate(this.name_err);
                u2 = cc.instantiate(this.syntax_err);
            }else{
                u1 = cc.instantiate(this.syntax_err);
                u2 = cc.instantiate(this.name_err);
            }

            r1.parent = this.enemybullet;
            r2.parent = this.enemybullet;
            r3.parent = this.enemybullet;
            l1.parent = this.enemybullet;
            l2.parent = this.enemybullet;
            l3.parent = this.enemybullet;
            l4.parent = this.enemybullet;
            u1.parent = this.enemybullet;
            u2.parent = this.enemybullet;

            r1.position = cc.v2(620,387);
            r2.position = cc.v2(660,284);
            r3.position = cc.v2(582,141);

            l1.position = cc.v2(274,380);
            l2.position = cc.v2(260,349);
            l3.position = cc.v2(215,246);
            l4.position = cc.v2(271,200);

            u1.position = cc.v2(507, 482);
            u2.position = cc.v2(557, 452);

            this.scheduleOnce(function(){
                r1.destroy();
                r2.destroy();
                r3.destroy();
                l1.destroy();
                l2.destroy();
                l3.destroy();
                l4.destroy();
                u1.destroy();
                u2.destroy();
            },1);

        },5);
    }
    update(){
        this.playerAnimation();
    }
    private playerAnimation(){

        if(this.animateState == null){
            this.animateState = this.anim.play('spider');
        }else if(this.animateState.name != 'spider'){
            this.animateState = this.anim.play('spider');
        }
    }
    
}