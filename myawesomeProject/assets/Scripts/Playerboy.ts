import GameManager from "./GameManager";
 
const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Playerboy extends cc.Component 
{
    @property()
    playerSpeed: number = 8000;
 
    @property()
    playerStandSpeed: number = 50;

    
    private idleFrame: cc.SpriteFrame = null;
    private anim: cc.Animation = null;
    private animateState = null;
    private moveDir = 0;
    private onGround: boolean = true;
    private life: number = 5;
    private score: number = 1000;
    private jump = 0;
    private canopen1=0;
    private canopen2=0;
    private canopen3=0;

    public move_r:boolean = false;
    public move_l:boolean = false;
    public move_j:boolean = false;
    // public calm:boolean = true;
    // public isDead:boolean = false;
    onload(){
        //cc.director.getCollisionManager().enabled = true;
        //cc.director.getPhysicsManager().enabled = true;
        this.anim = this.node.getComponent(cc.Animation);
        
    }
    start () {
        this.life = 5;
        this.score = 1000;
        this.idleFrame = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);
        this.schedule(function(){
            this.second -= 1;
            // this.time.getComponent(cc.Label).string = this.second.toString();
            if(this.second == 0){
                this.updatescore(this.score);
                cc.director.loadScene("gameover");
            }
        },1);
    }
 
    update(dt)
    {   var velocity = this.node.getComponent(cc.RigidBody).linearVelocity;
        
        this.node.x += this.playerSpeed * this.moveDir * dt;
        this.node.scaleX = (this.moveDir >= 0) ? 1 : -1;
       // if(this.onGround = false)
            this.node.y--;
        // if(this.move_r&& this.onGround)this.score += 1;
        
        this.playerAnimation();
        if(this.life!=5){
            this.scheduleOnce(function () {
                cc.audioEngine.stopMusic();
                // 这里的 this 指向 component
                // cc.find('over').active = true;
                //cc.game.restart();
                cc.director.loadScene('failed');
                cc.log("failed");
            }, 0.5);}
    }
    

    //give velocity 
   
    playerMove(moveDir: number)
    {
        this.moveDir = moveDir;
    }
    
    playerJump(velocity: number)
    {
       // cc.log(this.onGround);
        if(this.jump == 1)return;

        this.jump=1;
        cc.log("jump");
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, velocity);
    }

    onBeginContact(contact, self, other)
    {
        cc.log("collide");
        cc.log(other.node.name,other.node.group);
       if(other.node.group == "ground")
        {
            // if(this.isDead)this.isDead = false;
            if(contact.getWorldManifold().normal == "(0.00, -1.00)"){
                this.onGround = true;
                this.jump=0;
                
            }
            //else if(contact.getWorldManifold().normal == "(1.00, 0.00)"){
              //  this.onGround = true;
            //}
        }else if(other.node.group == "udfloor"){
            if(contact.getWorldManifold().normal == "(0.00, -1.00)"){
                this.onGround = true;
                this.jump=0;
                this.getComponent(cc.RigidBody).gravityScale = 0;
            }else{
                contact.disabled = true;
            }
            
        }
        switch (other.tag) {
            case 1://red
                this.redbar(other, self);
                break;
            case 2://blue
                this.bluebar(other, self);
                break;
            case 3://green
                this.greenbar(other, self);
                break;  
            case 5://door
                this.open1(other, self);
                break;  
            case 7://door
                this.open2(other, self);
                break; 
            case 9://door
                this.open3(other, self);
                break; 
            case 10://fan
                this.fan(other, self);
                 break; 
        }

    }
    fan(other, self){
        //this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 300);
        //this.node.y+=1;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 320);
        cc.log(this.node.y);
    }
    redbar(other, self){
        this.life--;
        cc.log(this.life);
    }
    bluebar(other, self){
        //if(other.node.group == "ground")
        //{
            // if(this.isDead)this.isDead = false;
            //if(contact.getWorldManifold().normal == "(0.00, -1.00)"){
                this.onGround = true;
                this.jump=0;
            //}
            //else if(contact.getWorldManifold().normal == "(1.00, 0.00)"){
              //  this.onGround = true;
            //}
       // }
       cc.log(this.life);
    }
    greenbar(other, self){//nobody
        this.life--;
        cc.log(this.life);
    }
    open1(other, self){//nobody
        this.canopen1=1;
        cc.log("this.open :"+ this.canopen1);
        
    }
    door1(){
        if(this.canopen1==1)
        return 1;

        else
        return 0;
    }
    open2(other, self){//nobody
        this.canopen2=1;
        cc.log("this.open :"+ this.canopen2);
        
    }
    door2(){
        if(this.canopen2==1)
        return 1;

        else
        return 0;
    }
    open3(other, self){//nobody
        this.canopen3=1;
        cc.log("this.open :"+ this.canopen3);
        
    }
    door3(){
        if(this.canopen3==1)
        return 1;

        else
        return 0;
    }
    onEndContact(contact, self, other)
    {
        cc.log(this.onGround);
        if(other.node.group == "ground" || other.node.group == "udfloor")
        {
            this.onGround = false;
            this.getComponent(cc.RigidBody).gravityScale = 0.35;
        }
    }
    
    playerAnimation()
    {
        this.node.scaleX = (this.move_l) ? -1 : (this.move_r) ? 1 : this.node.scaleX;

        
        if(this.onGround) // move animation can play only when shoot or jump animation finished
        {
            // if(this.jDown)
            //     this.animateState = this.anim.play('shoot');
            if(this.move_j)
            {
                    this.animateState = this.anim.play('jump');

                    this.playerJump(1);
            }
            else if(this.move_r || this.move_l)
            {
                if(this.animateState == null || this.animateState.name != 'move') // when first call or last animation is not move
                    this.animateState = this.anim.play('move');
            }
            else
            {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if(this.animateState == null || this.animateState.name != 'idle')
                    this.animateState = this.anim.play('idle');
            }
        }
    }


}