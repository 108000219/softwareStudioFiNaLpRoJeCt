// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }
    golevel1() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.audioEngine.stopMusic();
             cc.director.loadScene('level.1');
             cc.log("level.1");
         }, 0.5); 
     }
     golevel2() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.audioEngine.stopMusic();
             cc.director.loadScene('level.2');
             cc.log("level.2");
         }, 0.5); 
     }
     golevel3() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.audioEngine.stopMusic();
             cc.director.loadScene('level.3');
             cc.log("level.3");
         }, 0.5); 
     }
     golevel4() {
        
        
        this.scheduleOnce(function () {
            // 这里的 this 指向 component
            // cc.find('over').active = true;
            //cc.game.restart();
            cc.audioEngine.stopMusic();
            cc.director.loadScene('Monster');
            cc.log("Monster");
        }, 0.5); 
     }
     gomenu() {
        
        
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.audioEngine.stopMusic();
             cc.director.loadScene('menu');
             cc.log("menu");
         }, 0.5); 
     }
     goproblem() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             // cc.audioEngine.stopMusic();
             cc.director.loadScene('problem');
             cc.log("problem");
         }, 0.5); 
     }
     gostory1() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.director.loadScene('story1');
             cc.log("story1");
         }, 0.5); 
     }
     gostory2() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.director.loadScene("story2");
             cc.log("story2");
         }, 0.5); 
     }
     gostory3() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.director.loadScene("story3");
             cc.log("story3");
         }, 0.5); 
     }
     gostorym() {
        
        // this.sendValue();
         // do whatever you want with button
         // 另外，注意这种方式注册的事件，也无法传递 customEventData
         this.scheduleOnce(function () {
             // 这里的 this 指向 component
             // cc.find('over').active = true;
             //cc.game.restart();
             cc.director.loadScene("storym");
             cc.log("storym");
         }, 0.5); 
     }
    // update (dt) {}
}
