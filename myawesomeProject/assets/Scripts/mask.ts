import GameMgr_e from "./gameMgrEnemy";
const {ccclass, property} = cc._decorator;
 
@ccclass
export default class mask extends cc.Component 
{
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;
    @property(GameMgr_e)
    gameMgr: GameMgr_e = null;

    onload(){
        cc.director.getCollisionManager().enabled = true;
        this.gameMgr.enemylife = 50;
    }
    start(){
        let action1 = cc.moveBy(0.2, -10, 4);
        let action2 = cc.moveBy(0.2, 30, -2);
        let action3 = cc.moveBy(0.2, -20, -2);
        let action = cc.repeatForever(cc.sequence(action1, action2, action3));
        this.node.runAction(action);
    }
    
    onBeginContact(contact, self, other){
        cc.log("enemy been shooted");
        cc.log(other);
        cc.log(other.node.name);
        if(other.node.name == "bullet_player"){
            other.node.destroy();
            cc.audioEngine.playEffect(this.bgm, false);
            this.gameMgr.enemylife -= 1;
        }
    }
    
}