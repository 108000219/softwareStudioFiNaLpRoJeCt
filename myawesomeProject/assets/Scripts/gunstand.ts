import GameMgr_e from "./gameMgrEnemy";
import Playerboy_e from "./playerboy_e";
import Playergirl_e from "./playergirl_e";
import gunPlayer from "./gunplayer";

const {ccclass, property} = cc._decorator;
 
@ccclass
export default class gunStand extends cc.Component 
{

    @property(GameMgr_e)
    gameMgr: GameMgr_e = null; 
    @property(gunPlayer)
    player_gun: gunPlayer = null;
    @property(Playerboy_e)
    player_b: Playerboy_e = null;
    @property(Playergirl_e)
    player_g: Playergirl_e = null;
    
    


    public girlStand: boolean = false;
    public boyStand: boolean = false;


    onload(){
        cc.director.getCollisionManager().enabled = true;
        
    }

    update(){
        if(this.girlStand && this.boyStand){
            if(this.gameMgr.one == 2){
                this.player_gun.shoot();
            }
        }
    }
    

    onBeginContact(contact, self, other)
    {
        cc.log(other.node.name);
        if(other.node.name == "Girl")
        {
            this.girlStand = true;
        }else if(other.node.name == "Boy")
        {
            this.boyStand = true;
        }
    }

    onEndContact(contact, self, other)
    {
        
        if(other.node.name == "Girl")
        {
            this.girlStand = false;

        }else if(other.node.name == "Boy")
        {
            this.boyStand = false;

        }
    }

    
}