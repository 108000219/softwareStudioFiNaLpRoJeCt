import GameMgr_e from "./gameMgrEnemy";

const {ccclass, property} = cc._decorator;
 
@ccclass
export default class gunPlayer extends cc.Component 
{
    @property(GameMgr_e)
    gameMgr: GameMgr_e = null;
    
    @property(cc.Prefab)
    bullets: cc.Prefab = null;

    @property(cc.Node)
    Bullets: cc.Node = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    private calm: number = 0;

    onload(){
        cc.director.getCollisionManager().enabled = true;
    }
    shoot(){

        if(this.calm || this.gameMgr.bullet_count <= 0)return;
        let bullet = cc.instantiate(this.bullets);
        bullet.parent = this.Bullets;
        bullet.position = cc.v2(749,129);
        this.calm = 1;
        this.gameMgr.one = 0;
        this.gameMgr.bullet_count -= 1;
        cc.audioEngine.playEffect(this.bgm, false);
        this.scheduleOnce(function(){
            bullet.destroy();
            this.calm = 0;
        },1);
        this.scheduleOnce(function(){
            this.calm = 0;
        },0.8);
        cc.log("shoot!!!!!");

    }
    
}