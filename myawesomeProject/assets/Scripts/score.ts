import GameManager from "./GameManager";
const {ccclass, property} = cc._decorator;
 
@ccclass
export default class redDiamond extends cc.Component 
{
    
    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;
    
    

    onload(){
       // cc.director.getCollisionManager().enabled = true
       cc.audioEngine.playMusic(this.bgm, true);
    }
    start(){
        cc.audioEngine.playMusic(this.bgm, true);
            
       
    }
    
}