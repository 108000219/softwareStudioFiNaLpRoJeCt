const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Btnl extends cc.Component 
{
    @property(cc.Node)
    moveingFloor: cc.Node = null;
    
    
    @property(cc.Float)
    uppos: number = 0.0;
    @property(cc.Float)
    downpos: number = 0.0;
    private anim: cc.Animation = null;
    private animateState = null;
    private pushed = 0;
    onload(){
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.node.getComponent(cc.Animation);
    }
    start () {
        this.anim = this.getComponent(cc.Animation);
    }
    onBeginContact(contact, self, other)
    {
        
        if(other.node.group == "player")
        {
            cc.log(contact.getWorldManifold().normal);
            if(contact.getWorldManifold().normal.y > 0.8){
                this.animateState = this.anim.play('btnpress');
                let action1;
                let action2;
                action1 = cc.moveTo(2,this.moveingFloor.x, this.downpos);
                action2 = cc.moveBy(0.5,0, -8);
                this.moveingFloor.runAction(action1);
               // this.node.runAction(action2);
            }
        }
    }
    onEndContact(contact, self, other)
    {
        if(other.node.group == "player"){
            this.animateState = this.anim.play('btnleave');
            let action;
            let action1;
            action1 = cc.moveBy(0.2,0, 8);
            action = cc.moveTo(0.5,this.moveingFloor.x, this.uppos);
            this.moveingFloor.runAction(action);
           // this.node.runAction(action1);

        }
    }
    private afterPushed(){
        this.pushed = 1;
        this.animateState = this.anim.play('switchrl_e');
        cc.log("push anum end");
    }
}