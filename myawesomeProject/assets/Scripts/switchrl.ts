const {ccclass, property} = cc._decorator;
 
@ccclass
export default class Switch_rl extends cc.Component 
{
    @property(cc.Node)
    moveingFloor: cc.Node = null;
    private anim: cc.Animation = null;
    private animateState = null;
    private pushed = 0;
    onload(){
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.node.getComponent(cc.Animation);
    }
    start () {
        this.anim = this.getComponent(cc.Animation);
    }
    onBeginContact(contact, self, other)
    {
        
        if(other.node.group == "player")
        {
            cc.log(contact.getWorldManifold().normal);
            if(contact.getWorldManifold().normal.x > 0 && contact.getWorldManifold().normal.y == 0 && this.pushed == 0){
                this.animateState = this.anim.play('switchrl_m');
                this.anim.on('finished', this.afterPushed, this);
            }
        }
    }
    private afterPushed(){
        this.pushed = 1;
        this.animateState = this.anim.play('switchrl_e');
        cc.log("push anum end");
    }
}