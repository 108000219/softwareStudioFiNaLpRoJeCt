import GameMgr_e from "./gameMgrEnemy";
const {ccclass, property} = cc._decorator;
 
@ccclass
export default class redDiamond_m extends cc.Component 
{
    @property(GameMgr_e)
    gameMgr: GameMgr_e = null;
    
    onload(){
        cc.director.getCollisionManager().enabled = true;
    }
    onBeginContact(contact, self, other)
    {
        
        if(other.node.name == "Girl")
        {
            this.gameMgr.score += 50; 
            this.gameMgr.bullet_count +=1 ;
            cc.log("bullet:",this.gameMgr.bullet_count);
            this.node.active = false;

            this.schedule(function(){
                this.node.active = true;
            },15);

        }else{
            contact.disabled = true;
        }
    }
}